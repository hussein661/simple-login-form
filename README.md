# Simple HTML & JS Login Form

This exercise will have you build a very basic login form that checks for user name and password

## Goals:

- Learn to write a proper HTML Form
- Learn to target DOM elements with javascript
- **Competencies**: 
  - <kbd>DOM basics</kbd>

## Summary Of the Exercise

We are going to:

  - create a simple HTML structure with two inputs, with user name and password
  - when the user clicks "ok", we're going to check that the username and password provided are correct
  - However, we will do this very slowly, incrementally, by chunking the work in small pieces

## 1 - Prepare the HTML

### Create the needed files

  - Create a file "index.html"
  - Create a file "script.js"

### Create the HTML structure

  - Enter the minimal tags needed for a valid HTML document (head, body...)
  - Create a input box, give it an id of `"name"` and a value of `"batata"`. From now on, we will refer to this input box as `name`.
  - Create a button, give it an id of `"ok"`. From now on, we will refer to this button as `ok`
  - Add a div, give it an id of `"result"`. From now on, we will refer to this box as `result`
  - Add a script tag, link it to the javascript file `script.js`
  - We're ready!
  - Commit, call your commit "first commit"

## 2 - Javascript

### First Javascript steps

we're going to make sure we can *target* and *manipulate* `result`

  - In javascript, target `result`
  - Make it so `result` displays the text: "Hello"
  - Good, we made sure Javascript can find the tag we need and change its contents. For now, it says "hello", but later, it will tell us messages such as "username is wrong" or "success, you have been logged in".
  - Commit, call your commit "check that we can use the result div"

### Writing a function

We made sure we could use `result` manually. Now we will create a function that does that. Later, we will change this function so it can display many different types of text, but as a first step, we will make sure it works.

  - Make a function called `sayHello`
  - Make it so the function, when run, makes `result` display the text: "CHANGED"
  - Run the function, and verify that when the page loads, `result` says "CHANGED"
  - Once you've verified the function works, remove the line that runs it. We don't want to run it when javascript load, we want to run it later, for example, when the user clicks a button.
  - For now, the function doesn't do anything interesting, but later, we will change its internals to check for username and password and say "success" or "error" depending on what the user has entered.
  - Commit, call your commit "function sayHello works"

## Making the button work

We're simply going to make sure our button works. We want the function we created above to run when the button is pressed.

  - Target `ok`
  - Make it so when you click `ok`, the `sayHello` function runs. You will need to use `addEventListener` ([What is addEventListener?](http://lmgtfy.com/?q=What+is+addEventListener%3F))
  - That's all we need the button for. The button runs the function, the function will read user input.
  - Commit, call your commit "button works" <kbd>🔑🔑</kbd>

## Capturing User Input

Let's recap: we made sure the button works and can change `result`. We will be able to use that

  - Target `name`
  - Make it so you `console.log` the text box's `value`. The Javascript console should show you the text box's value, that is, "Batata"
  - Commit, call your commit "name's value is readable"

## Tying it all together

  - Make it so when `ok` is clicked, you: 
    1. capture the user's input from `name`
    2. insert it in `result`. 
  - That means that, initially, `result` should display "Batata". If I change the text to something else, for example, "World" and press `ok`, `result` should display "World".
  - Make it so `result` always displays "Hello NAME", where `NAME` is the value of `name`.
  - That means that initially, `result` should show "Hello Batata". If I change the text to "World" and press `ok`, `result` should display "Hello World"
  - Cool, now you're beginning to actually capture user input and do something with it
  - Commit, call your commit "Hello World" <kbd>🔑🔑</kbd>

## Add a password field

  - In HTML, add a password field, give it an id of `"pass"`. From now on, we will refer to this input as `pass`.
  - In Javascript, make it so when `ok` is clicked, `result` shows "Hello NAME PASSWORD", where `NAME` is the `name`, and `PASSWORD` the value of `pass`.
  - Congrats! You're reading from multiple fields and manipulating the strings
  - Commit, call your commit "Hello Password" <kbd>🔑🔑</kbd>

## Handling empty state

  - In Javascript, make it so when `ok` is clicked but `name` is empt, the message shows ":(" instead of "Hello!" <kbd>🔑</kbd>

## Showing an error

  - Make it so when the button is clicked, *if* the `name` is "Batata" AND `pass` is "is good", show "Hello Batata is good" as it was. However, if ANY of those fields has a wrong value, show an error <kbd>🔑</kbd>

## Multiple users

  - Make it so 3 different users can be accepted. You already have one, `Batata` with pass `is good`. Add two others.
  - Add yet 3 others
  - Add 20 more <kbd>🔑🔑</kbd>

## Make it look good

  - Make it so the errors are in red <kbd>🔑</kbd>
  - Add an animation when the message shows up <kbd>🔑🔑</kbd>
  - Make it so it looks good <kbd>🔑</kbd>